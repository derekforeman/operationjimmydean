import RPi.GPIO as GPIO
import time
import subprocess
import os, sys, getopt
from datetime import datetime
from netifaces import interfaces, ifaddresses

#Define a couple of variables
#The logfile
basedir = os.path.abspath(os.path.dirname(__file__))
now = datetime.now()
logfile = os.path.join(basedir, 'logs','OperationJimmyDean_{0}{1}{2}-{3}{4}{5}.log'.format(now.year, now.month, now.day, now.hour, now.minute, now.second))

#The sleep time. This can be overriden with command line param.
sleep_time = 30

#Debugging. This can be overridden with a command line param.
debug = False

def run_cam():
    global pause_time
    log_output("PIR Module Test (Ctrl+C to exit)")
    time.sleep(2)

    hipchat_message("Ready. Running at {0}".format(my_ip))

    while keep_running:
        if GPIO.input(PIR_PIN):
            #GPIO.output(PIR_PIN, GPIO.HIGH)
            log_output("Motion Detected!")
            now=datetime.now()
            filename="capture{0}{1}{2}-{3}{4}{5}.jpg".format(now.year, now.month, now.day, now.hour, now.minute, now.second)
            subprocess.call("/usr/bin/raspistill -w 1296 -h 972 -t 1 -e jpg -q 75 -o /home/motionTracking/%s" % filename, shell=True)
            log_output("Captured {0}".format(filename))
            #time.sleep(0.5)
            #GPIO.output(PIR_PIN, GPIO.LOW)
            if pause_time == sleep_time:
                pause_time = 1
                log_output("Motion detected. Setting the pause time to {0} second(s)!".format(pause_time))
        else:
            if pause_time != sleep_time:
                log_output("Motion seems to have stopped. Resetting the pause time back to {0} second(s)".format(sleep_time))
                pause_time = sleep_time

        time.sleep(pause_time)

        if pause_time == sleep_time:
            #Motion seems to have stopped. Let's do something with those pictures
            log_output("Attempting to do something with the pictures.")
            subprocess.call("/bin/sh /home/pi/bin/push_pics_to_s3.sh", shell=True, stdin=None, stdout=None, stderr=None, close_fds=True)

def usage():
    print("This command accepts the following options.")
    print("\t-h, --help: This usage message.")
    print("\t-d, --debug: Turn on console logging. Otherwise output will be sent to a file, as indicated at the top of the script")
    print("\t-s N, --sleep=N: Set the amount of time to sleep.")
    sys.exit(0)

def get_ip():
    tmp_ip = None
    while not tmp_ip:
        for ifaceName in interfaces():
            time.sleep(1)
            try:
                t = ifaddresses(ifaceName)[2][0]['addr']
                if t != '127.0.0.1':
                    tmp_ip = t
            except:
                # If an interface does not have an IP, then an exception will be caught here.
                pass
            
    return tmp_ip

def log_output(msg):
    if not debug:
        with open(logfile, "a") as log:
            log.write('{0}: {1}\n'.format(datetime.now(),msg))
    else:
        print(msg)

def hipchat_message(msg):
    subprocess.call("echo {0} | hipchat_room_message".format(msg), shell=True)

if __name__ == "__main__":
    # Setup the GPIO
    GPIO.setmode(GPIO.BCM)
    PIR_PIN=14
    GPIO.setup(PIR_PIN, GPIO.IN)

    # Check the command line options
    try:
        # Get the parameters and make them usable options
        opts, args = getopt.getopt(sys.argv[1:], "hds:", ["help","debug", "sleep="])

    except:
        usage()
        # Exit with 99
        sys.exit(99)

    for opt,arg in opts:
        if opt in ("-h", "--help"):
            usage()
            sys.exit(0)
        elif opt in ("-d", "--debug"):
            debug = True
        elif opt in ("-s", "--sleep"):
            try:
                sleep_time = int(arg)
            except ValueError:
                print ("Sleep must be an integer.")
                sys.exit(2)
        else:
            print("Unknown option: {0}".format(opt))
            sys.exit(1)

    #Get our IP address
    my_ip = get_ip()

    #Set the pause time.
    # This is used to actually control the wait time between motion checks. When there is motion, it changes to 2 seconds.
    pause_time = sleep_time

    #Set a variable to keep us running
    keep_running = True

    #and finally, let's run
    while keep_running:
        try:
            run_cam()
        except KeyboardInterrupt:
            user_input=raw_input("Caught keyboard interrupt. Do you really want to quit? (Y/N)")
            if user_input.lower() == 'n':
                pass
            else:
                keep_running = False
                GPIO.cleanup()
