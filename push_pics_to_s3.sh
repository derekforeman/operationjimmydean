#!/bin/bash

#set S3 bucket name here.
S3BUCKET=""
for f in $(ls ${HOME}/motion/*.jpg)
do
	FILE=$(echo ${f} | cut -d'/' -f4) 
	#echo ${f}
	s3cmd put ${f} s3://${S3BUCKET}/ 2>&1 >>/dev/null
	if [ $? -eq 0 ]
	then
		rm -f ${f}
		echo "Possible thief photo at http://${S3BUCKET}.s3.amazonaws.com/${FILE}" | hipchat_room_message
	fi	 
done
